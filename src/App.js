import { connect } from 'react-redux';
// import { useState } from 'react';
import './App.css';
import store from './store';

function App(props) {
  // let [todo, updateTodo] = useState(props.todo);

  function handleSubmit(event) {
    event.preventDefault();

    // Dispatch to add todo in a list

    props.dispatch({
      type: 'addToDo',
      payload: event.target.task.value,
      isCompleted: false,
    });
    event.target.task.value = '';
    
    // updateTodo([...todo, event.target.task.value]);
  }

  return (
    <div>
      <h1>React Redux To Do List </h1>
      <form onSubmit={handleSubmit}>
        <input type="text" placeholder="Enter To Do " name="task" />
        <input type="submit" value="Add To-Do" />
      </form>
      {store.getState().todo.map((singleToDo, index) => {
        return (
          <div key={index}>
            <h1>
              {index + 1}. {singleToDo}
            </h1>
          </div>
        );
      })}
    </div>
  );
}

function mapStateToProps(state) {
  return {
    todo: state.todo,
  };
}

export default connect(mapStateToProps)(App);
