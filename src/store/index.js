import { createStore } from 'redux';

function reducerFunction(state = { todo: [] }, action) {
  switch (action.type) {
    case 'addToDo':
      return { todo: [...state.todo, action.payload] };
    default:
      return state;
  }
}

let store = createStore(reducerFunction);

export default store;
